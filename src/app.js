import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Papa from 'papaparse';
import { Col, Button, MDBInput, MDBFileInput, MDBTable, MDBTableHead, MDBTableBody } from 'mdbreact';
import { ToastContainer, toast } from 'react-toastify';

import * as actions from './actions';

class App extends Component {

  state = {
    numberOfMonsters: 0,
    monsters: []
  }
  
  /**
   * Process the input file and parse the rows extracting the city and directions to other cities.
   */
  fileInputHandler = (value) => {

    let cities = [];
    let cityLocationIndex = 1;

    // Parse the input file
    Papa.parse(value[0], {
      step: function(row) {
        if (row.data[0] && row.data[0] != "") { // If the row isn't empty.
          try {
            let rowSegments = row.data[0][0].split(" "); // Split the row according to the city, and directions i.e Ege north=Dodala
            let length = rowSegments.length - 1; // Get the number of directions i.e. north, east, south, west.

            let i = 0; // Index counter for the direction segments.
            
            // City object consisting of an id to associate a location index of which is used for the monsters, as well as a name of the city, and possible directions.
            let city = {
              id: cityLocationIndex,
              name: rowSegments[0],
              directions: {
              },
            }

            // Identify the directions to other cities for a given city. Some cities may not always have 4 directions.
            for (i = 0; i <= length; i++) {
              let directionSegments = rowSegments[i].split("="); // For each of the direction segements split them to extract the direction, and city.

              switch (directionSegments[0]) {
                case "north":
                  city.directions.north = directionSegments[1]; // Assign the key north to city north of this current city.
                break;
                case "east":
                  city.directions.east = directionSegments[1]; // Assign the key east to city east of this current city.
                break;
                case "south":
                  city.directions.south = directionSegments[1]; // Assign the key south to city south of this current city.
                break;
                case "west":
                  city.directions.west = directionSegments[1]; // Assign the key west to city west of this current city.
                break;
                default:
                break;
              }
            }

            cityLocationIndex++; // Increment the unique location index where each city has a different id as such.

            cities.push(city); // Push the city on the cities array.

          } catch (err) {
            console.error(err);
          }
        }
      }, 
      complete: (results) => {
        this.props.addCities(cities); // Once completed parsing push the cities onto global state using redux and access via mapping state to props.
      }
    });
  }

  /**
   * Sets the input number of monsters in state.
   */
  numberInputHandler = (numberOfMonsters) => {
    this.setState({ numberOfMonsters })
  }

  /**
   * On submit, ths begins generating the number of monsters, and then moving the monsters in random locations, of which when landing in the same location results in fights, and thus deaths, and destroyed cities.
   */
  submit = () => {

    // There must be atleast 2 monsters. 
    if (this.state.numberOfMonsters < 2) {
      toast.error('We\'re going to need some more monsters!');
      return false;
    }

    // There must be cities.
    if (this.props.cities.length === 0) {
      toast.error('We\'re going to need some more cities!');
      return false;
    }

    let monsters = []; // An array to store the monsters.
    
    let i = 1;
    for (i = 0; i < this.state.numberOfMonsters; i++) {
      let monster = {
        id: i,
        locationIndex: Math.floor(Math.random() * this.props.cities.length + 1) // Randomly assign the location for the given monster. 
      }
      monsters.push(monster);
    }

    // While there is more than 1 monster and 1 city continue whie loop.
    while (monsters.length > 1 && this.props.cities.length > 1) {
      console.log('monsters', monsters.length, 'cities', this.props.cities.length);

      let randomMonster = Math.floor(Math.random() * monsters.length + 1) + -1 // Randomm Monster
      let currentLocation = this.props.cities.find(x => x.id === monsters[randomMonster].locationIndex); // Find current city of given random monster.

      if (currentLocation) {
        let shuffleDirections = _.shuffle(currentLocation.directions); // Shuffle the possible moves for a given city i.e. North, East, South, West.
        let pickedDirection = Math.floor(Math.random() * shuffleDirections.length) + 0 // Pick a random direction.
    
        let findCity = this.props.cities.find(x => x.name === shuffleDirections[pickedDirection]); // Find the given city of that picked direction.
        let pickedDirectionLocationIndex;
    
        if (findCity) {
          pickedDirectionLocationIndex = findCity.id; // Assign the location index of the city to assign onto the monster.
        }
  
      monsters[randomMonster].locationIndex = pickedDirectionLocationIndex; // Assign location index of city to monster.
  
      // Is another monster at this location ... loop through and check
      let monsterAtSameLocation = monsters.find(x => x.locationIndex === monsters[randomMonster].locationIndex); // Is there another monster at the same location as the current monster?
  
      // When two monsters end up in the same place, they fight, and in the
      // process kill each other and destroy the city.  When a city is
      // destroyed, it is removed from the map, and so are any roads that lead
      // into or out of it.
      
      // If monster is at the same location, check if it is not the current monster, and then 
      if (monsterAtSameLocation) {
        // Check that I am not the Monster.
        if (monsterAtSameLocation.id !== monsters[randomMonster].id) {
          if (findCity) {
            console.log(`${findCity.name} has been destroyed by monster ${monsters[randomMonster].id} and ${monsterAtSameLocation.id}`)
          }

          monsters = monsters.filter((value, index, arr) => {  
            return value.id != monsters[randomMonster].id && value.id != monsterAtSameLocation.id;
          });
  
          // Remove any paths to the given city using findCity.name
          if (findCity) {
            // Map over the cities removing any paths or directions to the city which has now been destroyed.
            _.map(this.props.cities, (values) => { 
              const { directions } = values;
              if (directions.north === findCity.name) {
                delete directions.north;
                this.props.editCity(values); // Update the city remove the key north
              } 
              else if (directions.east === findCity.name) {
                delete directions.east;
                this.props.editCity(values); // Update the city remove the key east
              }
              else if (directions.south === findCity.name) {
                delete directions.south
                this.props.editCity(values); // Update the city remove the key south
              } 
              else if (directions.west === findCity.name) {
                delete directions.west
                this.props.editCity(values); // Update the city remove the key west
              }
            })
            this.props.deleteCity(pickedDirectionLocationIndex); // Delete the destroyed city
          }
        }
      }
    }}

    // Display a notification to the user informing how many monsters are left.
    if (monsters.length > 1) {
      console.log(`There are ${monsters.length} monsters left!`)
      toast.info(`There are ${monsters.length} monsters left!`);
    } else if (monsters.length === 1) {
      console.log(`There is ${monsters.length} monster left!`)
      toast.info(`There is ${monsters.length} monster left!`);
    } else {
      console.log(`There are no monsters left!`)
      toast.info(`There are no monsters left!`);
    }

    console.log('Cities', this.props.cities);
  }

  render() {
    return (
      <div className="container">
        <ToastContainer 
          hideProgressBar={true}
          newestOnTop={true}
          autoClose={5000} />  
        <div>
          <h1>Monsters</h1>
          <MDBFileInput
            label="Upload a text file containing the map." 
            getValue={this.fileInputHandler}
          />
          <MDBInput
            type="number" 
            label="How many monsters would you like to create?"
            getValue={this.numberInputHandler}
          />
          <Button color="primary" onClick={() => this.submit()}>Fight!!!</Button>

          <MDBTable responsive autoWidth align="center">
            <MDBTableHead>
              <tr>
                <th>index</th>
                <th>City</th>
                <th>North</th>
                <th>East</th>
                <th>South</th>
                <th>West</th>
              </tr>
            </MDBTableHead>
            <MDBTableBody>
              {this.props.cities.map((city, i) => {
                const { id, name, directions } = city;
                return (
                  <tr key={id}>
                    <td>{id}</td>
                    <td>{name}</td>
                    <td>{directions.north}</td>
                    <td>{directions.east}</td>
                    <td>{directions.south}</td>
                    <td>{directions.west}</td>
                  </tr>
                )
              })}
            </MDBTableBody>
          </MDBTable>
          {this.props.cities.length === 0 &&
            <p>There are no currently no cities. Have you imported a map?</p>
          }
        </div>
      </div>
    );
  }
}

function mapStateToProps({ cities }) {
  return {
    cities: cities.cities
  }
}

export default connect(mapStateToProps, actions)(App);
