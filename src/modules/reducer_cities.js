import _ from 'lodash';
import {
  ADD_CITIES,
  EDIT_CITY,
  DELETE_CITY,
} from '../actions/types';

const INITIAL_STATE = {
  cities: [],
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case ADD_CITIES:
        return { ...state, cities: action.payload };
      case EDIT_CITY:
        let tmp = state.cities.findIndex(r => r.id === action.payload.id);
        let tmp2 = state.cities.slice();
        tmp2[tmp] = {...tmp2[tmp], ...action.payload};
      case DELETE_CITY:
        return {...state, cities: state.cities.filter((o) => o.id !== action.payload)}
      default:
        return state;
    }
  }