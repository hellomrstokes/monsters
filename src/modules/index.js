import { persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import CITIESReducer from './reducer_cities';

const config = {
    key: 'primary',
    storage
}

const monsterElectron = persistCombineReducers(config, {
    cities: CITIESReducer,
})

export default monsterElectron;