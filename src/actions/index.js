import { ipcRenderer } from 'electron';
import {ADD_CITIES, EDIT_CITY, DELETE_CITY } from "./types";

export const addCities = (cities) => (dispatch) => {
    console.log('addCities', cities);
    dispatch({ type: ADD_CITIES, payload: cities });
}

export const editCity = (city) => (dispatch) => {
    console.log('editCity', city);
    dispatch({ type: EDIT_CITY, payload: city });
}

export const deleteCity = (id) => (dispatch) => {
    console.log('deleteCity', id);
    dispatch({ type: DELETE_CITY, payload: id });
}